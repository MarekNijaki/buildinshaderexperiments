Shader "Examples/Vertex with custom data in surface shader" 
{
    Properties 
    {
      _MainTex ("Texture", 2D) = "white" {}
      _BumpMap ("Bumpmap", 2D) = "bump" {}
    }
    
    // The example below defines a custom 'float3 customColor' member, which is computed in a 'vertex function'.
    // It is possible to use a “vertex modifier” function that will modify the incoming vertex data in the vertex Shader. 
    // It is also possible to use a “vertex modifier” function, to compute custom data.
    // 'vertex:functionName' function should take two parameters: 'inout appdata_full' and 'out Input'.
    // You can fill in any Input member that is not a built-in value there.
    // Note: Custom Input members used in this way must not have names beginning with ‘uv’ or they won’t work properly.
    // This can be useful for better optimization, eg it’s possible to compute 'Rim' lighting at the GameObject’s vertices, 
    // instead of doing that in the Surface Shader per-pixel
    SubShader 
    {
      Tags { "RenderType" = "Opaque" }
        
      CGPROGRAM

      // Define usage of surface shader with labert lighting and additional vertex shader.
      #pragma surface SurfShader Lambert vertex:VertShader

      // Input structure.
      struct Input
      {
          // Main texture.
          float2 uv_MainTex;
          // Normal / Bump texture.
          float2 uv_BumpMap;
          // Custom color.
          float3 customColor;
      };

      // Variables.
      sampler2D _MainTex;
      sampler2D _BumpMap;

      // Vertex shader (with additional custom data).
      void VertShader(inout appdata_full v, out Input o)
      {
          // Initialize output with surface shader input data.
          UNITY_INITIALIZE_OUTPUT(Input,o);
          // Set custom color value.
          o.customColor = abs(v.normal);
      }

      // Surface shader.
      void SurfShader(Input IN, inout SurfaceOutput o)
      {
          // Set albedo color (main color of mesh).
          o.Albedo = tex2D (_MainTex, IN.uv_MainTex).rgb * 0.5;
          // Apply custom color over existing albedo color.
          o.Albedo *= IN.customColor;          

          // Set bumps.
          o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
      }
      
      ENDCG
    } 
    
    Fallback "Diffuse"
  }