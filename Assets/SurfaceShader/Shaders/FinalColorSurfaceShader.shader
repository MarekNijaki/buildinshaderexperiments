Shader "Examples/Final color surface shader" 
{
    Properties 
    {
      _MainTex ("Texture", 2D) = "white" {}
      _BumpMap ("Bumpmap", 2D) = "bump" {}
      _ColorTint ("Tint", Color) = (1.0, 0.6, 0.6, 1.0)
    }
    
    // It is possible to use a “final color modifier” function that will modify the final color computed by the Shader.
    // The Surface Shader compilation directive 'finalcolor:functionName' is used for this.
    // That function takes 'Input IN, SurfaceOutput o, inout fixed4 color' parameters.
    // Here’s a simple Shader that applies tint to the final color. 
    // This is different from just applying tint to the surface Albedo color. 
    // Tint will also affect any color that comes from Lightmaps, Light Probes and similar extra sources.
    SubShader 
    {
      Tags { "RenderType" = "Opaque" }
        
      CGPROGRAM

      // Define usage of surface shader with labert lighting and final color function.
      #pragma surface SurfaceShader Lambert finalcolor:ApplyFinalColor

      // Input structure.
      struct Input
      {
          // Main texture.
          float2 uv_MainTex;
          // Normal / Bump texture.
          float2 uv_BumpMap;
      };

      // Variables.
      sampler2D _MainTex;
      sampler2D _BumpMap;
      fixed4 _ColorTint;

      // Final color function.
      void ApplyFinalColor(Input IN, SurfaceOutput o, inout fixed4 color)
      {
          color *= _ColorTint;
      }

      // Surface shader.
      void SurfaceShader(Input IN, inout SurfaceOutput o)
      {
          // Set albedo color (main color of mesh).
          o.Albedo = tex2D (_MainTex, IN.uv_MainTex).rgb * 0.5;

          // Set bumps.
          o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
      }
      
      ENDCG
    } 
    
    Fallback "Diffuse"
  }