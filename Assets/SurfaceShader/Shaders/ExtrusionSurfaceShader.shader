Shader "Examples/Extrusion surface shader" 
{
    Properties 
    {
      _MainTex ("Texture", 2D) = "white" {}
      _BumpMap ("Bumpmap", 2D) = "bump" {}
      _Amount ("Extrusion Amount", Range(-0.03, 0.03)) = 0.03
    }
    
    // It is possible to use a “vertex modifier” function that will modify the incoming vertex data in the vertex Shader. 
    // This can be used for things like procedural animation and extrusion along normals. 
    // Surface Shader compilation directive 'vertex:functionName' is used for that, 
    // with a function that takes inout 'appdata_full' parameter.
    SubShader 
    {
      Tags { "RenderType" = "Opaque" }
        
      CGPROGRAM

      // Define usage of surface shader with labert lighting and additional vertex shader.
      #pragma surface SurfaceShader Lambert vertex:VertShader

      // Input structure.
      struct Input
      {
          // Main texture.
          float2 uv_MainTex;
          // Normal / Bump texture.
          float2 uv_BumpMap;
      };

      // Variables.
      sampler2D _MainTex;
      sampler2D _BumpMap;
      float _Amount;

      // Vertex shader.
      void VertShader(inout appdata_full v)
      {
          // Extrude verticies perpendicular to normals.
          v.vertex.xyz += v.normal * _Amount;
      }

      // Surface shader.
      void SurfaceShader(Input IN, inout SurfaceOutput o)
      {
          // Set albedo color (main color of mesh).
          o.Albedo = tex2D (_MainTex, IN.uv_MainTex).rgb * 0.5;

          // Set bumps.
          o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
      }
      
      ENDCG
    } 
    
    Fallback "Diffuse"
  }