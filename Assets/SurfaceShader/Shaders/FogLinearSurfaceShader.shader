Shader "Examples/Linear fog surface shader" 
{
    Properties 
    {
      _MainTex ("Base (RGB)", 2D) = "white" {}
    }
    
    // A common case for using final color modifier is implementing completely custom Fog in forward rendering.
    // Fog needs to affect the final computed pixel Shader color, which is exactly what the final color modifier does.
    // This combines the vertex modifier with the custom vertex data (fog) and the final color modifier.    
    SubShader 
    {
      Tags { "RenderType" = "Opaque" }

      // ???
      LOD 200
        
      CGPROGRAM

      // Define usage of surface shader, with labert lighting, vertex shader and final color function.
      #pragma surface SurfShader Lambert finalcolor:ApplyFinalColor vertex:VerShader
      // ???
      #pragma multi_compile_fog

      // Input structure.
      struct Input
      {
          // Main texture.
          float2 uv_MainTex;
          // ???
          half fog;
      };

      // Variables.
      sampler2D _MainTex;
      uniform half4 unity_FogStart;
      uniform half4 unity_FogEnd;

      // Vertex shader.
      void VerShader(inout appdata_full v, out Input data)
      {
          // Initialize output with surface shader input data.
          UNITY_INITIALIZE_OUTPUT(Input,data);
          // 
          float pos = length(UnityObjectToViewPos(v.vertex).xyz);
          float diff = unity_FogEnd.x - unity_FogStart.x;
          float invDiff = 1.0f / diff;
          data.fog = clamp((unity_FogEnd.x - pos) * invDiff, 0.0, 1.0);
      }

      // Final color function.
      void ApplyFinalColor(Input IN, SurfaceOutput o, inout fixed4 color)
      {
          #ifdef UNITY_PASS_FORWARDADD
            UNITY_APPLY_FOG_COLOR(IN.fog, color, float4(0,0,0,0));
          #else
            UNITY_APPLY_FOG_COLOR(IN.fog, color, unity_FogColor);
          #endif
      }

      // Surface shader.
      void SurfShader(Input IN, inout SurfaceOutput o)
      {
          half4 c = tex2D (_MainTex, IN.uv_MainTex);
          o.Albedo = c.rgb;
          o.Alpha = c.a;
      }
      
      ENDCG
    } 
    
    Fallback "Diffuse"
  }