Shader "Examples/Fog surface shader" 
{
    Properties 
    {
      _MainTex ("Texture", 2D) = "white" {}
      _BumpMap ("Bumpmap", 2D) = "bump" {}
      _FogColor ("Fog Color", Color) = (0.3, 0.4, 0.7, 1.0)
      _FogThickness("Fog thickness", Range(0.0, 1.0)) = 1.0
    }
    
    // A common case for using final color modifier is implementing completely custom Fog in forward rendering.
    // Fog needs to affect the final computed pixel Shader color, which is exactly what the final color modifier does.
    // Here’s a Shader that applies fog tint based on the distance from screen center.
    // This combines the vertex modifier with the custom vertex data (fog) and the final color modifier. 
    // When used in the forward rendering additive pass, the Fog needs to fade to black.
    // This example handles that and performs a check for 'UNITY_PASS_FORWARDADD'.
    SubShader 
    {
      Tags { "RenderType" = "Opaque" }
        
      CGPROGRAM

      // Define usage of surface shader, with labert lighting, vertex shader and final color function.
      #pragma surface SurfShader Lambert finalcolor:ApplyFinalColor vertex:VerShader

      // Input structure.
      struct Input
      {
          // Main texture.
          float2 uv_MainTex;
          // Normal / Bump texture.
          float2 uv_BumpMap;
          // Distance to the center of the screen (fog data).
          half distanceToScreenCenter;
      };

      // Variables.
      sampler2D _MainTex;
      sampler2D _BumpMap;
      fixed4 _FogColor;
      fixed _FogThickness;

      // Vertex shader.
      void VerShader(inout appdata_full v, out Input data)
      {
          // Initialize output with surface shader input data.
          UNITY_INITIALIZE_OUTPUT(Input,data);
          // "Clip space" is a step along the way of transforming vertices in your mesh to pixel coordinates.
          // Every vertex that will is on the screen(is in clip space) has coordinates in the range '-1.0' to '+1.0'.
          // It's called clip space because at this point the GPU can cut off ("clip") shapes that won't be visible.
          // See: http://www.opengl-tutorial.org/beginners-tutorials/tutorial-3-matrices/
          // See: https://www.scratchapixel.com/lessons/3d-basic-rendering/computing-pixel-coordinates-of-3d-point/mathematics-computing-2d-coordinates-of-3d-points
          float4 clipPos = UnityObjectToClipPos(v.vertex);
          clipPos.xy = clipPos.xy / clipPos.w;
          // Dot returns between '-1.0' and '1.0'.
          float distanceToScreenCenter = dot(clipPos.xy, clipPos.xy);
          // This is the same as line above. I put it only to show how 'Dot()' product works.
          float cosOfTwoSameVectors = 1.0;
          distanceToScreenCenter = length(clipPos.xy) * length(clipPos.xy) * cosOfTwoSameVectors;
          // Idk why those calculation above, i simplified it.
          distanceToScreenCenter = length(clipPos.xy);
          // Compute distance (fog data).
          data.distanceToScreenCenter = min(1,  distanceToScreenCenter * 0.5);
          data.distanceToScreenCenter = min(1,  distanceToScreenCenter * _FogThickness);
      }

      // Final color function.
      void ApplyFinalColor(Input IN, SurfaceOutput o, inout fixed4 color)
      {
          fixed3 fogColor = _FogColor.rgb;

          // When fog is used in the forward rendering additive pass, it needs to fade to black.
          #ifdef UNITY_PASS_FORWARDADD
          fogColor = 0;
          #endif

          // Lerp from input color of pixel to fog color, based of distance to the center.
          color.rgb = lerp(color.rgb, fogColor, IN.distanceToScreenCenter);
      }

      // Surface shader.
      void SurfShader(Input IN, inout SurfaceOutput o)
      {
          // Set albedo color (main color of mesh).
          o.Albedo = tex2D (_MainTex, IN.uv_MainTex).rgb;

          // Set bumps.
          o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
      }
      
      ENDCG
    } 
    
    Fallback "Diffuse"
  }