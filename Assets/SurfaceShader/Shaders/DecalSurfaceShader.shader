Shader "Examples/Decal surface shader" 
{
    Properties 
    {
      _MainTex ("Base (RGB)", 2D) = "white" {}
    }
    
    // Decals are commonly used to add details to Materials at run time (for example, bullet impacts).
    // They are especially useful in 'deferred' rendering, because they alter the 'GBuffer' before it is lit, 
    // therefore saving on performance.
    // In a typical scenario, Decals should be rendered after the opaque objects and should not be shadow casters.
    SubShader 
    {
      // Render after opaque objects. Decals should not cas shadows.
      Tags { "RenderType"="Opaque" "Queue"="Geometry+1" "ForceNoShadowCasting"="True" }

      // ???
      LOD 200
      
      // ???  
      Offset -1, -1
        
      CGPROGRAM

      // Define usage of surface shader, with labert lighting, and decals mode set to blend.
      // Semitransparent decal shader.
      // This is meant for objects that lie atop of other surfaces, and use alpha blending
      #pragma surface SurfaceShader Lambert decal:blend

      // Input structure.
      struct Input
      {
          // Main texture.
          float2 uv_MainTex;
      };

      // Variables.
      sampler2D _MainTex;

      // Surface shader.
      void SurfaceShader(Input IN, inout SurfaceOutput o)
      {
        half4 tex = tex2D(_MainTex, IN.uv_MainTex);
        o.Albedo = tex.rgb;
        o.Alpha = tex.a;
      }
      
      ENDCG
    } 
    
    Fallback "Diffuse"
  }