Shader "Examples/Slice surface shader" 
{
    Properties 
    {
      _MainTex ("Texture", 2D) = "white" {}
      _BumpMap ("Bumpmap", 2D) = "bump" {}
      _SliceFrequency ("Slice frequency", Range(0.0, 10.0)) = 5.0
      _SliceFrequencyX ("Slice frequency X", Range(0.0, 10.0)) = 5.0
      _SliceFrequencyY ("Slice frequency Y", Range(0.0, 10.0)) = 5.0
      _SliceFrequencyZ ("Slice frequency Z", Range(0.0, 10.0)) = 5.0
      _SlicePercentageCutoff ("Slice percetage cutoff", Range(0.0, 1.0)) = 0.3
    }
    
    // Here’s a Shader that  “slices” the GameObject by discarding pixels in nearly horizontal rings. 
    // It does this by using the 'clip()' Cg/HLSL function based on the world position of a pixel. 
    // We’ll use the built-in 'worldPos' Surface Shader variable.
    SubShader 
    {
      Tags { "RenderType" = "Opaque" }
      
      // Turn off culling.
      Cull Off
        
      CGPROGRAM
      
      // Define usage of surface shader with labert lighting.
      #pragma surface SurfaceShader Lambert

      // Input structure.
      struct Input
      {
          // Main texture.
          float2 uv_MainTex;
          // Normal / Bump texture.
          float2 uv_BumpMap;
          // World space position.
          float3 worldPos;
      };

      // Variables.
      sampler2D _MainTex;
      sampler2D _BumpMap;
      float _SliceFrequency;
      float _SliceFrequencyX;
      float _SliceFrequencyY;
      float _SliceFrequencyZ;
      float _SlicePercentageCutoff;

      // Surface shader.
      void SurfaceShader(Input IN, inout SurfaceOutput o)
      {
          // Simple slice.
          // float mask = frac(IN.worldPos.y * _SliceFrequency) - _SlicePercentageCutoff;

          // Slice with diffrent angles.
          float mask = frac(IN.worldPos.x*_SliceFrequencyX + 
                            IN.worldPos.y*_SliceFrequencyY + 
                            IN.worldPos.z*_SliceFrequencyZ) - _SlicePercentageCutoff;          
          // Clip will discard pixels where mask is 0.0 or below.
          clip(mask);

          // Set albedo color (main color of mesh).
          o.Albedo = tex2D (_MainTex, IN.uv_MainTex).rgb * 0.5;

          // Set bumps.
          o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
      }
      
      ENDCG
    } 
    
    Fallback "Diffuse"
  }