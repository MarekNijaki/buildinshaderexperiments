Shader "Examples/Reflections surface shader" 
{
    Properties 
    {
      _MainTex ("Texture", 2D) = "white" {}
      _BumpMap ("Bumpmap", 2D) = "bump" {}
      _Cube ("Cubemap", CUBE) = "" {}
    }
    
    // Here’s a Shader that does cubemapped reflection using built-in worldRefl input.
    SubShader 
    {
      Tags { "RenderType" = "Opaque" }
        
      CGPROGRAM
      
	  // Define usage of surface shader with labert lighting.
      #pragma surface SurfaceShader Lambert

      // Input structure.
      struct Input
      {
          // Main texture.
          float2 uv_MainTex;
          // Normal / Bump texture.
          float2 uv_BumpMap;
          // World reflection vector if surface shader does not write to 'o.Normal'.
          // See Reflect-Diffuse shader for example.
          // Commented because we use 'o.Normal'.
          //float3 worldRefl;
          // World reflection vector if surface shader writes to 'o.Normal'.
          // To get the reflection vector based on per-pixel normal map, use 'WorldReflectionVector(IN, o.Normal)'.
          float3 worldRefl; INTERNAL_DATA
      };

      // Variables.
      sampler2D _MainTex;
      sampler2D _BumpMap;
      samplerCUBE _Cube;

      // Surface shader.
      void SurfaceShader(Input IN, inout SurfaceOutput o)
      {
          // Set albedo color (main color of mesh).
          o.Albedo = tex2D (_MainTex, IN.uv_MainTex).rgb * 0.5;

          // Set bumps.
          o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
          
          // Because cubemap reflection color is assigned to Emission, we get a very shiny soldier.
          //o.Emission = texCUBE(_Cube, IN.worldRefl).rgb;

          // 'WorldReflectionVector()' is defined by the surface shader translator.
          // You can see its value by using adding '#pragma debug' and then checking out the generated Cg,
          // by clicking "Open Compiled Shader" in the Inspector
          float3 reflectionVector = WorldReflectionVector(IN, o.Normal);
          o.Emission = texCUBE(_Cube, reflectionVector).rgb;
      }
      
      ENDCG
    } 
    
    Fallback "Diffuse"
  }