Shader "Examples/Simple surface shader" 
{
  Properties 
  {
    _MainTex ("Texture", 2D) = "white" {}
    _IsNormalEnabled ("Is normal map enabled", Range(0.0, 1.0)) = 0.0
    _Normal ("Normal/BumpMap", 2D) = "normal" {}
    _IsBumpMapEnabled ("Is bump map enabled", Range(0.0, 1.0)) = 0.0
    _BumpMap ("Bumpmap", 2D) = "bump" {}
    _IsRimEnabled ("Is rim enabled", Range(0.0, 1.0)) = 0.0
    _RimColor ("Rim Color", Color) = (0.26,0.19,0.16,0.0)
    _RimPower ("Rim Power", Range(0.5,8.0)) = 3.0
    _IsDetailTexEnabled ("Is detail enabled", Range(0.0, 1.0)) = 0.0
    _Detail ("Detail texture", 2D) = "gray" {}
    _IsScreenSpaceDetailEnabled("Is screen detail enabled", Range(0.0, 1.0)) = 0.0
  }

  SubShader 
  {
    // Set render type to solid objects
    Tags { "RenderType" = "Opaque" }

    CGPROGRAM

      // Set target grapics API to 3.0 to be able to use more properties inside shader (there is limitation of 8 properties).
      #pragma target 3.0
    
      // Define usage of surface shader with labert lighting.
      #pragma surface SurfaceShader Lambert

      // Input structure for surface shader.
      // Surface Shader input structure: https://docs.unity3d.com/2021.2/Documentation/Manual/SL-SurfaceShaders.html
      struct Input
      {
        // Texture coordinates must be named “uv” followed by texture name.
        float2 uv_MainTex;
        // UV normal texture (holds information about veritcies normals).
        float2 uv_Normal;
        // Same as above, just diffrent name (you can treat it as alias for above). 
        float2 uv_BumpMap;
        // View direction (direction of given vertex towards camera).
        float3 viewDir;
        // UV detail texture(used to add more details on top of main texture).
        float2 uv_Detail;
        // Screen space position for reflection or screenspace effects.
        float4 screenPos;
        // World reflection vector if surface shader does not write to 'o.Normal'.
        float3 worldRefl;
        // Simple interpolated per-vertex color.
        float4 color : COLOR;
      };

      // Variables definitions for properties.
      // Samlers: https://docs.unity3d.com/2021.2/Documentation/Manual/SL-SamplerStates.html
      // Using sampler2D, sampler3D, samplerCUBE HLSL keywords declares both texture and sampler.
      // Unity allows declaring textures and samplers using DX11-style HLSL syntax, with a special naming convention to match them up:
      // samplers that have names in the form of “sampler”+TextureName will take sampling states from that texture.
      sampler2D _MainTex;
      float _IsNormalEnabled;
      sampler2D _Normal;
      float _IsBumpMapEnabled;
      sampler2D _BumpMap;
      float _IsRimEnabled;
      float4 _RimColor;
      float _RimPower;
      float _IsDetailTexEnabled;
      sampler2D _Detail;
      float _IsScreenSpaceDetailEnabled;
      
      // List of available variables in output structure : https://docs.unity3d.com/2021.2/Documentation/Manual/SL-SurfaceShaders.html
      void SurfaceShader(Input IN, inout SurfaceOutput o)
      {
        // Set albedo color (base color for whole model).
        // o.Albedo = 1;

        // Set texture on mesh.
        o.Albedo = tex2D(_MainTex, IN.uv_MainTex).rgb;
        
        // Set normal/bumpmap texture on mesh.
        if(_IsNormalEnabled > 0.0)
        {
          // 'Tex2D()' gets(sample) a color value from the texture.
          // It is 4 values (rgba), so float4.
          // Sampling is needed to get tiling, offset, wrap mode and etc?
          float4 textureSample = tex2D(_Normal, IN.uv_Normal);
          o.Normal = UnpackNormal(textureSample);
        }
        if(_IsBumpMapEnabled > 0.0)
        {
          o.Normal = UnpackNormal(tex2D (_BumpMap, IN.uv_BumpMap));
        }

        // Set rim light to higlight edges of a game object.
        // Rim light get stronger with higher angle between normals and view direction (eg. 89, 90, 91 degrees).
        if(_IsRimEnabled > 0.0)
        {
          float3 normalizedVierDir = normalize(IN.viewDir);
          // Dot product returns value between '1.0' and '-1.0'.
          // Value depend on angle between reference vector and some other vector.
          // If vectors points in the same direction, value of dot() is '1.0'
          // If angle is 90 degrees, value of dot() is '0.0'.
          // If angle is 180 degrees, value of dot() is '-1.0'.
          half angleFactor = dot(normalizedVierDir, o.Normal);
          // saturate() is same as clamp(0.0, 1.0).
          // We clamp 'angleFactor' so faces that are pointing opposite to 'vieDir' (backfaces)
          // will not create stronger rim light, but normal rim light.
          half normalisedAngle = saturate(angleFactor);
          half rim = 1.0 - normalisedAngle;
          // Apply rim light to emmision.
          o.Emission = _RimColor.rgb * pow (rim, _RimPower);
        }

        if(_IsDetailTexEnabled > 0.0)
        {
          o.Albedo *= tex2D (_Detail, IN.uv_Detail).rgb * 2;
        }

        // Set texture in screen space.
        if(_IsScreenSpaceDetailEnabled > 0.0)
        {
          // Link: https://forum.unity.com/threads/what-is-screenpos-w.616003/
          // 'w' in 'screenPos' vector is a perspective divide (depth from camera towords given object face/plane).
          // Debug to see what exactly is 'w' parameter.
          // if(IN.screenPos.w < 1.5 && IN.screenPos.w > -1.5) o.Albedo = float4(1, 0.0, 0.0, 0.0);

          float2 screenUV;
          if(IN.screenPos.w == 0.0)
          {
             screenUV = IN.screenPos.xy;            
          }
          else
          {
            screenUV = IN.screenPos.xy / IN.screenPos.w;            
          }
          screenUV *= float2(8,6);
          o.Albedo *= tex2D(_Detail, screenUV).rgb * 2;
        }
      }
    
    ENDCG
  }

  Fallback "Diffuse"
}