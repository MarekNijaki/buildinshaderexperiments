Shader "Examples/Toon Ramp lighting surface shader" 
{
    Properties 
    {
        _MainTex ("Texture", 2D) = "white" {}
        _BumpMap ("Bumpmap", 2D) = "bump" {}
        _Ramp ("Ramp", 2D) = "bump" {}
    }
    
    SubShader 
    {
        Tags { "RenderType" = "Opaque" }
        
        CGPROGRAM

        // Define usage of surface shader with custom labert lighting function.
        #pragma surface SurfaceShader ToonRamp

        // Variables.
        sampler2D _Ramp;
        
        // Custom lighting model.
        // The following example shows a “Ramp” lighting model that uses a Texture ramp to define
        // how surfaces respond to the angles between the light and the normal.
        // It’s especially effective when used with Toon lighting.
        half4 LightingToonRamp(SurfaceOutput s, half3 lightDir, half atten)
        {
            // Compute lighting by calculating a dot product between surface normal and light directio.
            half normalDotLightDir = dot(s.Normal, lightDir);
            // Set minmum diffuse to '0.5'. Rest come from 'Dot()' product. Maximum is '1.0'.
            // This addotional '0.5' will be only visible in places where 'Dot()' product had low value
            // (near edges of object, where angle between normals and light are very steep).
            half diffuse = normalDotLightDir * 0.5 + 0.5;
            // Get
            half3 ramp = tex2D(_Ramp, float2(diffuse,diffuse)).rgb;
            // Apply light color and light attenuation.
            // Attenuation is the coefficient of light energy loss as it travels through space.
            half4 color;
            color.rgb = s.Albedo * _LightColor0.rgb * ramp * atten;
            color.a = s.Alpha;
            // Return color.
            return color;
        }

        // Input structure.
        struct Input
        {
            // Main texture.
            float2 uv_MainTex;
            // Normal / Bump texture.
            float2 uv_BumpMap;
        };

        // Variables.
        sampler2D _MainTex;
        sampler2D _BumpMap;

        // Surface shader.
        void SurfaceShader(Input IN, inout SurfaceOutput o)
        {
            // Set albedo color (main color of mesh).
            o.Albedo = tex2D(_MainTex, IN.uv_MainTex).rgb;

            // Set bumps.
            o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
        }
        
        ENDCG
    }
    
    Fallback "Diffuse"
}