Shader "Examples/Custom Labert lighting surface shader" 
{
    Properties 
    {
        _MainTex ("Texture", 2D) = "white" {}
        _BumpMap ("Bumpmap", 2D) = "bump" {}
    }
    
    SubShader 
    {
        Tags { "RenderType" = "Opaque" }
        
        CGPROGRAM

        // Define usage of surface shader with custom labert lighting function.
        #pragma surface SurfaceShader CustomLambert

        // Custom Lambert lighting model.
        half4 LightingCustomLambert(SurfaceOutput s, half3 lightDir, half atten)
        {
            // Compute lighting by calculating a dot product between surface normal and light directio.
            half normalDotLightDir = dot(s.Normal, lightDir);
            // Apply light color and light attenuation.
            // Attenuation is the coefficient of light energy loss as it travels through space.
            half4 color;
            color.rgb = s.Albedo * _LightColor0.rgb * (normalDotLightDir * atten);
            color.a = s.Alpha;
            // Return color.
            return color;
        }

        // Input structure.
        struct Input
        {
            // Main texture.
            float2 uv_MainTex;
            // Normal / Bump texture.
            float2 uv_BumpMap;
        };

        // Variables.
        sampler2D _MainTex;
        sampler2D _BumpMap;

        // Surface shader.
        void SurfaceShader(Input IN, inout SurfaceOutput o)
        {
            // Set albedo color (main color of mesh).
            o.Albedo = tex2D (_MainTex, IN.uv_MainTex).rgb;

            // Set bumps.
            o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
        }
        
        ENDCG
    }
    
    Fallback "Diffuse"
}